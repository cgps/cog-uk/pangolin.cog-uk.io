# pangolin.cog-uk.io
The development of a methodology to assign names to lineages of SARS-CoV-2 using
a dynamic nomenclature that was previously described in this [bioRxiv article](https://www.biorxiv.org/content/10.1101/2020.04.17.046086v1) and
[post](http://virological.org/t/a-dynamic-nomenclature-proposal-for-sars-cov-2-to-assist-genomic-epidemiology/458) on virological.org.

pangolin (Phylogenetic Assignment of Named Global Outbreak LINeages), the
software used to assign lineages, is open source and can be found on github.
It was developed with the motivation to make it as easy as possible for labs to
obtain useful information from genome sequencing of SARS-CoV-2.

For those familiar with the UNIX command line the installation of this software
is straightforward requiring just a Python interpreter to run pangolin and the
conda package management system to install dependencies.
However for those who are unfamiliar with the command line or do not have access
to a UNIX computer, a web based application has been developed to allow users to
1. Assign lineages to genome sequences of SARS-CoV-2
2. View descriptive characteristics of the assigned lineage(s)
3. View the placement of the lineage in a phylogeny of global samples
4. View the temporal and geographic distribution of the assigned lineage(s)

See https://pangolin.docs.cog-uk.io for full documentation 

For anny issues please use this [link](https://gitlab.com/cgps/cog-uk/pangolin.cog-uk.io/-/issues)
